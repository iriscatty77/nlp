#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 27 20:15:56 2018

@author: changliu
"""

import nltk
from nltk import *

list1 = 'We had a nice party yesterday'.lower().split()
list2 = 'She came to visit me two days ago'.lower().split()
list3 = 'You may go now'.lower().split()
list4 = 'Their kids are not always naive'.lower().split()
list5 = "Nice kids came two days ago".lower().split()
list6 = "She had a party two days ago".lower().split()
list7 = "You came to are not".lower().split()

print("")
print("")
print("****************** part1 **********************")
print("")
print("")

HW3_grammar = nltk.CFG.fromstring("""
  S -> NP VP 
  VP -> V NP ADVP | V NP NP | V TO VP | V ADVP | MD VP | V RB ADVP ADJ
  ADVP -> NP RB | RB
  ADJ -> "nice" | "naive" 
  V -> "had"| "came" | "visit" | "go" | "are" 
  NP ->  Prop | Det NP |ADJ N | N | CD N
  Prop -> "I" | "we" | "she" | "me" |"you" 
  Det -> "a" | "their"
  CD -> "two"
  N -> "party" | "yesterday" | "days" | "kids" 
  TO -> "to"
  RB -> "ago" | "now" | "always" | "not"
  MD -> "may"
  """)

# make a recursive descent parser and parse the sentence
rdHW3_parser = nltk.RecursiveDescentParser(HW3_grammar)
print("##############list 1###############")
for tree in rdHW3_parser.parse(list1):
	print (tree)
print("##############list 2###############")
for tree in rdHW3_parser.parse(list2):
	print (tree)
print("##############list 3###############")
for tree in rdHW3_parser.parse(list3):
	print (tree)
print("##############list 4###############")
for tree in rdHW3_parser.parse(list4):
	print (tree)

print("##############list 5###############")
for tree in rdHW3_parser.parse(list5):
	print (tree)

print("##############list 6###############")
for tree in rdHW3_parser.parse(list6):
	print (tree)
    
print("##############list 7###############")
for tree in rdHW3_parser.parse(list7):
	print (tree)


print("")
print("")
print("****************** part2 **********************")
print("")
print("")

HW3_ProbGrammar = nltk.PCFG.fromstring("""
  S -> NP VP [1.0]
  VP -> V NP ADVP [0.125] | V NP NP [0.125]| V TO VP [0.25] 
  VP -> V ADVP [0.25]| MD VP [0.125]| V RB ADVP ADJ [0.125]
  ADVP -> NP RB [0.25]| RB [0.75]
  ADJ -> "nice" [0.5]| "naive" [0.5]|
  V -> "had" [0.2]| "came" [0.2]| "visit" [0.2]| "go" [0.2]| "are" [0.2]
  NP ->  Prop [0.33] | Det NP [0.22] |ADJ N [0.11] | N [0.22] | CD N [0.12]
  Prop ->  "we" [0.25]| "she" [0.25]| "me" [0.25]|"you" [0.25]
  Det -> "a" [0.5] | "their" [0.5]
  CD -> "two"[1.0]
  N -> "party" [0.25]| "yesterday" [0.25]| "days" [0.25]| "kids" [0.25] 
  TO -> "to" [1.0]
  RB -> "ago" [0.25]| "now" [0.25]| "always" [0.25]| "not"[0.25]
  MD -> "may" [1.0]
  """)


print("##############list 1###############")
viterbi_parser = nltk.ViterbiParser(HW3_ProbGrammar)
for tree in viterbi_parser.parse(list1):
    print (tree)
    
print("##############list 2###############")
viterbi_parser = nltk.ViterbiParser(HW3_ProbGrammar)
for tree in viterbi_parser.parse(list2):
    print (tree)
 
print("##############list 3###############")
viterbi_parser = nltk.ViterbiParser(HW3_ProbGrammar)
for tree in viterbi_parser.parse(list3):
    print (tree)

print("##############list 4###############")   
viterbi_parser = nltk.ViterbiParser(HW3_ProbGrammar)
for tree in viterbi_parser.parse(list4):
    print (tree)
 
print("##############list 5###############")
viterbi_parser = nltk.ViterbiParser(HW3_ProbGrammar)
for tree in viterbi_parser.parse(list5):
    print (tree)

print("##############list 6###############") 
viterbi_parser = nltk.ViterbiParser(HW3_ProbGrammar)
for tree in viterbi_parser.parse(list6):
    print (tree)
 
print("##############list 7###############")
viterbi_parser = nltk.ViterbiParser(HW3_ProbGrammar)
for tree in viterbi_parser.parse(list7):
    print (tree)