#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 21 03:03:46 2018

@author: changliu
"""


import datetime
import time

ts = time.time()
start = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
print(start)

import nltk
from nltk import *
from nltk.corpus import PlaintextCorpusReader
import re

##part2 preprocessing (20%)
mycorpus = PlaintextCorpusReader('.','.*\.txt',encoding='utf-8')
rawFile = mycorpus.raw("baby.txt")

myarray = rawFile.split("\r\n\r\nreviewerID:")

regexp = re.compile('reviewTime:.. .., 2010')
reviews2010 = [x for x in myarray if regexp.search(x)]



pattern = 'reviewText:.*'
ptoken = re.compile(pattern)

reviewText = [re.findall(ptoken, x)[0].replace("\r","") for x in reviews2010]

outputFile = open("output.txt","w+",encoding="utf-8")
##count = 0
for review in reviewText:
    ##if('reviewText:' in review):
        ##count=count+1
    ##outputFile.write(str(count)+'\n')    
    outputFile.write(review+'\n')
 
    
##test = reviewText[:100]
sentence=[]
for text in reviewText:
    text=text.replace("reviewText:","")
    sent = nltk.sent_tokenize(text)
    for s in sent:
        s=re.sub('\?|\.|\!|\/|\;|\:|\[|\]|\{|\}','',s)
        sentence.append(s)


##Sentiment analysis(80%)

from nltk.corpus import sentence_polarity
import random

######################################################################

#training the algorithm, 5-fold cross Validation for 8 algorithms
#takes a long time!!!!!! (>1 hr)
#please skip to line 389 if you don't want to spend so much time running
##but you need to run definition for SL and Combined_features before going to line 389

###start training

######################################################################
##unigram traning
sentences = sentence_polarity.sents()
sentence_polarity.categories()
documents = [(sent, cat) for cat in sentence_polarity.categories() 
	for sent in sentence_polarity.sents(categories=cat)]
random.shuffle(documents)

all_words_list = [word for (sent,cat) in documents for word in sent]
all_words = nltk.FreqDist(all_words_list)
word_items = all_words.most_common(2000)
word_features = [word for (word, freq) in word_items] 



def document_features(document, word_features):
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains({})'.format(word)] = (word in document_words)
    return features

featuresets = [(document_features(d,word_features), c) for (d,c) in documents]


print("baseline accuracy")
sum=0.0;
for i in range(0,5):
    if i==0:
        train_set, test_set = featuresets[2000:], featuresets[:2000]
    else:
        train_set, test_set = featuresets[:2000*i-1]+featuresets[2000*(i+1)+1:], featuresets[2000*i:2000*(i+1)]
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    sum+=nltk.classify.accuracy(classifier, test_set)

print("5-fold cross validation:")
print(sum/5)

##not feature set

negationwords = ['no', 'not', 'never', 'none', 'nowhere', 'nothing', 'noone', 'rather', 'hardly', 'scarcely', 'rarely', 'seldom', 'neither', 'nor']


def NOT_features(document, word_features, negationwords):
    features = {}
    for word in word_features:
        features['contains({})'.format(word)] = False
        features['contains(NOT{})'.format(word)] = False
    # go through document words in order
    for i in range(0, len(document)):
        word = document[i]
        if ((i + 1) < len(document)) and ((word in negationwords) or (word.endswith("n't"))):
            i += 1
            features['contains(NOT{})'.format(document[i])] = (document[i] in word_features)
        else:
            features['contains({})'.format(word)] = (word in word_features)
    return features

featuresets = [(NOT_features(d, word_features, negationwords), c) for (d, c) in documents]


print("negation accuracy")
sum=0.0;
for i in range(0,5):
    if i==0:
        train_set, test_set = featuresets[2000:], featuresets[:2000]
    else:
        train_set, test_set = featuresets[:2000*i-1]+featuresets[2000*(i+1)+1:], featuresets[2000*i:2000*(i+1)]
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    sum+=nltk.classify.accuracy(classifier, test_set)

print("5-fold cross validation:")
print(sum/5)

##classifier.show_most_informative_features(30)


##Subjectivity Count features

def readSubjectivity(path):
    flexicon = open(path, 'r')
    # initialize an empty dictionary
    sldict = { }
    for line in flexicon:
        fields = line.split()   # default is to split on whitespace
        # split each field on the '=' and keep the second part as the value
        strength = fields[0].split("=")[1]
        word = fields[2].split("=")[1]
        posTag = fields[3].split("=")[1]
        stemmed = fields[4].split("=")[1]
        polarity = fields[5].split("=")[1]
        if (stemmed == 'y'):
            isStemmed = True
        else:
            isStemmed = False
        # put a dictionary entry with the word as the keyword
        #     and a list of the other values
        sldict[word] = [strength, posTag, isStemmed, polarity]
    return sldict


SLpath = 'subjclueslen1-HLTEMNLP05.tff'
SL = readSubjectivity(SLpath)

def SL_features(document, word_features, SL):
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains({})'.format(word)] = (word in document_words)
    # count variables for the 4 classes of subjectivity
    weakPos = 0
    strongPos = 0
    weakNeg = 0
    strongNeg = 0
    for word in document_words:
        if word in SL:
            strength, posTag, isStemmed, polarity = SL[word]
            if strength == 'weaksubj' and polarity == 'positive':
                weakPos += 1
            if strength == 'strongsubj' and polarity == 'positive':
                strongPos += 1
            if strength == 'weaksubj' and polarity == 'negative':
                weakNeg += 1
            if strength == 'strongsubj' and polarity == 'negative':
                strongNeg += 1
            features['positivecount'] = weakPos + (2 * strongPos)
            features['negativecount'] = weakNeg + (2 * strongNeg)      
    return features

featuresets = [(SL_features(d, word_features, SL), c) for (d,c) in documents]


print("subjectivity accuracy")
sum=0.0;
for i in range(0,5):
    if i==0:
        train_set, test_set = featuresets[2000:], featuresets[:2000]
    else:
        train_set, test_set = featuresets[:2000*i-1]+featuresets[2000*(i+1)+1:], featuresets[2000*i:2000*(i+1)]
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    sum+=nltk.classify.accuracy(classifier, test_set)

print("5-fold cross validation:")
print(sum/5)




######trial#############

def Combined_features(document, word_features, negationwords,SL):
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains({})'.format(word)] = (word in document_words)
    # count variables for the 4 classes of subjectivity
    weakPos = 0
    strongPos = 0
    weakNeg = 0
    strongNeg = 0
    for word in document_words:
        if word in SL:
            strength, posTag, isStemmed, polarity = SL[word]
            if strength == 'weaksubj' and polarity == 'positive':
                weakPos += 1
            if strength == 'strongsubj' and polarity == 'positive':
                strongPos += 1
            if strength == 'weaksubj' and polarity == 'negative':
                weakNeg += 1
            if strength == 'strongsubj' and polarity == 'negative':
                strongNeg += 1
            features['positivecount'] = weakPos + (2 * strongPos)
            features['negativecount'] = weakNeg + (2 * strongNeg)   
            
    for i in range(0, len(document)):
        word = document[i]
        if ((i + 1) < len(document)) and ((word in negationwords) or (word.endswith("n't"))):
            i += 1
            features['contains(NOT{})'.format(document[i])] = (document[i] in word_features)
        else:
            features['contains({})'.format(word)] = (word in word_features)
    
    return features

##SL+negation###########

featuresets = [(Combined_features(d, word_features, negationwords, SL), c) for (d,c) in documents]


print("combined accuracy")
sum=0.0;
for i in range(0,5):
    if i==0:
        train_set, test_set = featuresets[2000:], featuresets[:2000]
    else:
        train_set, test_set = featuresets[:2000*i-1]+featuresets[2000*(i+1)+1:], featuresets[2000*i:2000*(i+1)]
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    sum+=nltk.classify.accuracy(classifier, test_set)

print("5-fold cross validation:")
print(sum/5)



##stop word filtering##############

##all-word featureset

def document_features(document, word_features):
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains({})'.format(word)] = (word in document_words)
    return features

stopwords = nltk.corpus.stopwords.words('english')
newstopwords = [word for word in stopwords if word not in negationwords]


new_all_words_list = [word for word in all_words_list if word not in newstopwords]
new_all_words = nltk.FreqDist(new_all_words_list)
new_word_items = new_all_words.most_common(2000)
new_word_features = [word for (word,count) in new_word_items]

featuresets = [(document_features(d,new_word_features), c) for (d,c) in documents]


print("base - stopword accuracy")
sum=0.0;
for i in range(0,5):
    if i==0:
        train_set, test_set = featuresets[2000:], featuresets[:2000]
    else:
        train_set, test_set = featuresets[:2000*i-1]+featuresets[2000*(i+1)+1:], featuresets[2000*i:2000*(i+1)]
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    sum+=nltk.classify.accuracy(classifier, test_set)

print("5-fold cross validation:")
print(sum/5)

##negation featureset

featuresets = [(NOT_features(d,new_word_features,negationwords), c) for (d,c) in documents]


print("negation - stopword accuracy")
sum=0.0;
for i in range(0,5):
    if i==0:
        train_set, test_set = featuresets[2000:], featuresets[:2000]
    else:
        train_set, test_set = featuresets[:2000*i-1]+featuresets[2000*(i+1)+1:], featuresets[2000*i:2000*(i+1)]
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    sum+=nltk.classify.accuracy(classifier, test_set)

print("5-fold cross validation:")
print(sum/5)

##SL featureset

featuresets = [(SL_features(d,new_word_features,SL), c) for (d,c) in documents]


print("SL - stopword accuracy")
sum=0.0;
for i in range(0,5):
    if i==0:
        train_set, test_set = featuresets[2000:], featuresets[:2000]
    else:
        train_set, test_set = featuresets[:2000*i-1]+featuresets[2000*(i+1)+1:], featuresets[2000*i:2000*(i+1)]
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    sum+=nltk.classify.accuracy(classifier, test_set)

print("5-fold cross validation:")
print(sum/5)

##combined featureset


featuresets = [(Combined_features(d,new_word_features,negationwords,SL), c) for (d,c) in documents]


print("combined - stopword accuracy")
sum=0.0;
for i in range(0,5):
    if i==0:
        train_set, test_set = featuresets[2000:], featuresets[:2000]
    else:
        train_set, test_set = featuresets[:2000*i-1]+featuresets[2000*(i+1)+1:], featuresets[2000*i:2000*(i+1)]
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    sum+=nltk.classify.accuracy(classifier, test_set)

print("5-fold cross validation:")
print(sum/5)



######################################################################

#training the algorithm, 5-fold cross Validation for 8 algorithms
#takes a long time!!!!!! (>1 hr)
#please skip if you don't want to spend so much time running

### end training

######################################################################

##Choose the best algorithm
##combined: SL+negation###########

featuresets = [(Combined_features(d, word_features, negationwords, SL), c) for (d,c) in documents]
train_set, test_set = featuresets[2000:], featuresets[:2000]
classifier = nltk.NaiveBayesClassifier.train(train_set)




##precision-recall-f-score
from nltk.metrics import *

reflist = []
testlist = []
for (features, label) in test_set:
    reflist.append(label) 
    testlist.append(classifier.classify(features))
    
refpos = set()
refneg = set()
testpos = set()
testneg = set()

for i, label in enumerate(reflist):
    if label == 'pos': refpos.add(i)
    if label == 'neg': refneg.add(i)

for i, label in enumerate(testlist):
    if label == 'pos': testpos.add(i)
    if label == 'neg': testneg.add(i)


## Define a function that calls the 3 NLTK functions
## to get precision, recall and F-measure

def printmeasures(label, refset, testset):
    print(label, 'precision:', precision(refset, testset))
    print(label, 'recall:', recall(refset, testset)) 
    print(label, 'F-measure:', f_measure(refset, testset))

## Print out the scores that show the model performance for each label

printmeasures('pos', refpos, testpos)
printmeasures('neg', refneg, testneg)


##apply the classifier to the reviews

pos=[]
neg=[]
for sent in sentence:
    texttokens = nltk.word_tokenize(sent)
    inputfeatureset = document_features(texttokens, word_features)
    res = classifier.classify(inputfeatureset)
    if res=='pos':
        pos.append(sent)
    else:
        neg.append(sent)
        
NumberPos = len(pos)
NumberNeg = len(neg)

percentPos = NumberPos/(NumberPos+NumberNeg)
percentNeg = NumberNeg/(NumberPos+NumberNeg)

print("percentage of positive sentences:")
print(percentPos)

print("percentage of negative sentences:")
print(percentNeg)



ts = time.time()
end = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
print(end)

















