#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
NSF Abstrct Analysis

Author: Chang Liu


Note: please put this .py file into the folder where the abstracts are. 
The output will appear in the same folder once it finished running.
"""



import nltk
from nltk import *
from nltk.corpus import PlaintextCorpusReader
import re
import os
files = os.listdir()
##for analysis of the result
maxAward = 0
minAward = 1000000000
sumAward = 0
OrgCount = {}
OrgAmount = {}
maxSentCount = 0
minSentCount = 100000
sumSentCount = 0
########part2##############
def preprocess(file):
  
    global maxAward,minAward, sumAward
##finding ('File')
    pattern = 'File        : [a-z]\d+'
    ptoken = re.compile(pattern)
    foundFile = re.findall(ptoken, file) 

    pattern2 = '[a-z]\d+'
    ptoken2 = re.compile(pattern2)
    fileName = re.findall(ptoken2, foundFile[0]) [0]
##finding ('NSF Org')
    pattern3 = 'NSF Org     : [A-Z]+'
    ptoken3 = re.compile(pattern3)
    foundOrg = re.findall(ptoken3, file) 

    pattern4 = '[A-Z]+'
    ptoken4 = re.compile(pattern4)
    Org = re.findall(ptoken4, foundOrg[0]) [2]
    ##for analysis of the result
    OrgCount[Org] = OrgCount.get(Org,0)+1
##finding ('Total Amt')
    pattern5 = 'Total Amt.  : \$\d+'
    ptoken5 = re.compile(pattern5)
    foundAmt = re.findall(ptoken5, file) 

    pattern6 = '\d+'
    ptoken6 = re.compile(pattern6)
    Amt = re.findall(ptoken6, foundAmt[0])[0]
    ##for analysis of the result
    if(int(Amt)>maxAward):
        maxAward = int(Amt)
    if(int(Amt)<minAward):
        minAward = int(Amt)
    sumAward+=int(Amt)
    OrgAmount[Org] = OrgCount.get(Org,0)+int(Amt)
##finding ('Abstract')
    pattern7 = 'Abstract    :.*'
    ptoken7 = re.compile(pattern7,re.DOTALL)
    foundAbs = re.findall(ptoken7, file) 

    pattern8 = ':.*'
    ptoken8 = re.compile(pattern8,re.DOTALL)
    foundAbs2 = re.findall(ptoken8, foundAbs[0])[0]
    
    Abs = " ".join(foundAbs2.split()).replace(': ','')
    
    return fileName+'  '+'  '+Org+'  '+'$'+Amt+'  '+Abs
    



mycorpus = PlaintextCorpusReader('.','.*\.txt',encoding='latin-1')
##file0 = mycorpus.raw(files[0])

##preprocess(file0)

outputFile = open("outputPart2.txt","w+",encoding="utf-8")

for file in files:
    if(file!='HW2.py' and file!='outputPart2.txt' and file!='outputPart3.txt'and file!='.DS_Store'):
        ##print(file)
        rawFile = mycorpus.raw(file)
        if(rawFile!=''):
            ##preprocess(rawFile)
            outputFile.write(preprocess(rawFile)+'\n')

##for analysis of the result
avgAward = sumAward/(len(files)-4)
maxValue = 0
minValue = 1000000
for key, value in OrgAmount.items():
    if value>maxValue:
        maxValue = value
    if value<minValue and value!=2:
        minValue = value
        
        
        
########part3##############
def preprocess2(file):
    
    
##finding ('File')
    pattern = 'File        : [a-z]\d+'
    ptoken = re.compile(pattern)
    foundFile = re.findall(ptoken, file) 

    pattern2 = '[a-z]\d+'
    ptoken2 = re.compile(pattern2)
    fileName = re.findall(ptoken2, foundFile[0]) [0]


##finding ('Abstract')

    pattern7 = 'Abstract    :.*'
    ptoken7 = re.compile(pattern7,re.DOTALL)
    foundAbs = re.findall(ptoken7, file) 

    pattern8 = ':.*'
    ptoken8 = re.compile(pattern8,re.DOTALL)
    foundAbs2 = re.findall(ptoken8, foundAbs[0])[0]
    
    Abs = " ".join(foundAbs2.split()).replace(': ','')
    
##sentence tokenize
    
    sent_text = nltk.sent_tokenize(Abs)
    return (fileName,sent_text)


#######main###########

outputFile = open("outputPart3.txt","w+",encoding="utf-8")
outputFile.write('Abstract_ID | Sentence_No | Sentence\n')

for file in files:
    if(file!='HW2.py' and file!='outputPart3.txt' and file!='outputPart2.txt'and file!='.DS_Store'):
        ##print(file)
        rawFile = mycorpus.raw(file)
        if(rawFile!=''):
            File = preprocess2(rawFile)[0]
            SentList = preprocess2(rawFile)[1]
            ##for analysis of the result#######
            if(len(SentList)>maxSentCount):
                maxSentCount=len(SentList)
            if(len(SentList)<minSentCount):
                minSentCount=len(SentList)
            sumSentCount += len(SentList)
            ###################################
            index = 0
            for sent in SentList:
                outputFile.write(File+'|'+str(index+1)+'|'+sent+'\n')
                index += 1
            outputFile.write('Number of Sentences : '+ str(index) + '\n' + '\n')

##for analysis of the result#######
avgSentCount = sumSentCount/(len(files)-4)

                
                
                
                