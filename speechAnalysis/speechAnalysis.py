#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 16 14:30:25 2018

@author: changliu
"""
import nltk

from nltk.corpus import PlaintextCorpusReader
from nltk import *
mycorpus = PlaintextCorpusReader('.','.*\.txt')

##commonly used function:
def alpha_filter(w):
    pattern = re.compile('^[^a-z]+$')
    if (pattern.match(w)):
        return True
    else:
        return False

###################txt1 analysis####################

hw1Srting1 = mycorpus.raw('state_union_part1.txt')
string1Tokens = nltk.word_tokenize(hw1Srting1)

##remove non-word tokens and lower case
alphaString1 = [w for w in string1Tokens if w.isalpha()]
lowerString1 = [w.lower() for w in alphaString1]

## remove stop words
stopwords = nltk.corpus.stopwords.words('english')
stoppedString1 = [w for w in lowerString1 if w not in stopwords]

##lemmatization
wnl = nltk.WordNetLemmatizer()
lemmaString1 = [wnl.lemmatize(t) for t in stoppedString1]

fdist = FreqDist(lemmaString1)
topkeys = fdist.most_common(50)
topkeysNormalized =[(pair[0],pair[1]/len(lemmaString1)*100) for pair in topkeys]

print('***********string1 top 50 raw**************')
for pair in topkeysNormalized:
    print(pair)


##bigram
str1bigrams = list(nltk.bigrams(lowerString1))
str1bigrams[:20]

bigram_measures = nltk.collocations.BigramAssocMeasures()
finder = BigramCollocationFinder.from_words(lowerString1)
finder.apply_word_filter(alpha_filter)
finder.apply_word_filter(lambda w: w in stopwords)


scored = finder.score_ngrams(bigram_measures.raw_freq)
print('***********string1 top 50 bigram**************')
for bscore in scored[:50]:
   print (bscore)


finder.apply_freq_filter(25)
scored = finder.score_ngrams(bigram_measures.pmi)
print('***********string1 top 50 bigram pmi **************')
for bscore in scored[:50]:
    print (bscore)




###################txt2 analysis####################


hw1Srting2 = mycorpus.raw('state_union_part2.txt')
string1Tokens = nltk.word_tokenize(hw1Srting2)

##remove non-word tokens and lower case
alphaString2 = [w for w in string1Tokens if w.isalpha()]
lowerString2 = [w.lower() for w in alphaString2]

## remove stop words
stopwords = nltk.corpus.stopwords.words('english')
stoppedString2 = [w for w in lowerString2 if w not in stopwords]

##lemmatization
wnl = nltk.WordNetLemmatizer()
lemmaString2 = [wnl.lemmatize(t) for t in stoppedString2]

fdist = FreqDist(lemmaString2)
topkeys = fdist.most_common(50)
topkeysNormalized =[(pair[0],pair[1]/len(lemmaString2)*100) for pair in topkeys]

print('***********string2 top 50 raw**************')
for pair in topkeysNormalized:
    print(pair)


##bigram
str2bigrams = list(nltk.bigrams(lowerString2))
str2bigrams[:20]

bigram_measures = nltk.collocations.BigramAssocMeasures()
finder2 = BigramCollocationFinder.from_words(lowerString2)
finder2.apply_word_filter(alpha_filter)
finder2.apply_word_filter(lambda w: w in stopwords)


scored = finder2.score_ngrams(bigram_measures.raw_freq)
print('***********string2 top 50 bigram**************')
for bscore in scored[:50]:
   print (bscore)


finder2.apply_freq_filter(25)
scored = finder2.score_ngrams(bigram_measures.pmi)
print('***********string2 top 50 bigram pmi **************')
for bscore in scored[:50]:
    print (bscore)





















